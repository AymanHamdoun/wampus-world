import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;

public class Launcher extends Application
{
	public static void main(String[] args)
	{
		launch(args);	
	}

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		Canvas canvas = new Canvas(750, 400);
		GraphicsContext gContext = canvas.getGraphicsContext2D();
		
		Group layout = new Group();
		layout.getChildren().add(canvas);
			
		Wampus wampusGame = new Wampus(gContext);
	
		Scene scene = new Scene(layout, 750, 400);
		scene.setOnMouseClicked(e->wampusGame.handleClick(e.getX(), e.getY(), e.getButton() == MouseButton.PRIMARY));
		
		primaryStage.setTitle("Wampus Game | Ayman");
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.setResizable(false);
		primaryStage.show();
		
		
	}
}
