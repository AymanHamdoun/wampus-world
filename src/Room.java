import java.util.ArrayList;

public class Room
{
	private ArrayList<Facts> facts = new ArrayList<>();
	private boolean visited;
	
	public Room(Facts fact)
	{
		facts.add(fact);
		visited = false;
	}
	
	public void addFact(Facts fact)
	{
		if(facts.contains(fact))
			return;
		
		if(facts.contains(Facts.Clear))
			facts.remove(Facts.Clear);
		
		if(facts.contains(Facts.Unknown))
			facts.remove(Facts.Unknown);
		
		facts.add(fact);
	}
	
	public void removeFact(Facts fact)
	{
		facts.remove(fact);
		
		if(facts.size()==0)
			facts.add(Facts.Clear);
	}
	
	public boolean hasFact(Facts fact)
	{
		return this.facts.contains(fact);
	}
	
	
	public ArrayList<Facts> getFacts()
	{
		return facts;
	}
	
	public boolean isEmpty()
	{
		return (facts.contains(Facts.Clear) || facts.contains(Facts.Unknown)) && facts.size() == 1;
	}
	
	public void setVisited(Boolean b)
	{
		visited = b;
	}
	
	public boolean isVisited()
	{
		return visited;
	}
	
	
	public String toString()
	{
		String print = "";
		for(Facts fact: this.facts)
		{
			switch (fact)
			{
				case Clear:
					print += "-Clear\n";
					break;
					
				case Wampus:
					print += "-Wampus\n";
					break;
					
				case Pit:
					print += "-Pit\n";
					break;
					
				case Stench:
					print += "-Stench\n";
					break;
					
				case Breeze:
					print += "-Breeze\n";
					break;
					
				case Gold:
					print += "-Gold\n";
					break;
				
				case Glitter:
					print += "-Glitter\n";
					break;
					
				default:
					break;
			}
		}
		return print;
	}
	
	public void copy(Room room)
	{
		this.facts = room.getFacts();
	}
}
