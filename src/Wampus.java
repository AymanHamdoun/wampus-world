import java.util.Random;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

class Wampus
{
	private Room[][] world = new Room[4][4];
	private Room[][] knowledgeBoard = new Room[4][4];
	private GraphicsContext graphicsContext;
	private byte playerX, playerY, bullets = 1;

	private Boolean[][] pits = new Boolean[4][4];
	private Boolean[][] wampus = new Boolean[4][4];
	private Boolean[][] gold = new Boolean[4][4];
	
	private Boolean playerDead = false;
	
	public Wampus(GraphicsContext graphicsContext)
	{
		this.graphicsContext = graphicsContext;
		initialize();
	}
	
	private void initialize()
	{
		for(int y = 0; y < 4; y++)
			for(int x = 0; x < 4; x++)
			{
				world[y][x] = new Room(Facts.Clear);
				knowledgeBoard[y][x] = new Room(Facts.Unknown); 
				pits[y][x] = true; 
				wampus[y][x] = true; 
				gold[y][x] = true; 
			}
		
		generateEntities();
		printGameBoard();
		printKnowledgeBoard();
		printBullets();
	}
	
	private void generateEntities()
	{
		byte pits = 0, wampus = 0, gold = 0, player = 0;
		Random random = new Random();
		byte x,y;
		
		do
		{
			x = (byte) random.nextInt(4);
			y = (byte) random.nextInt(4);
			
			if(pits < 3 && world[y][x].isEmpty())
			{
				world[y][x].addFact(Facts.Pit);
				pits++;
			}
			else if(wampus < 1 && world[y][x].isEmpty())
			{
				world[y][x].addFact(Facts.Wampus);
				wampus++;
			}
			else if(gold < 1 && world[y][x].isEmpty())
			{
				world[y][x].addFact(Facts.Gold);
				gold++;
			}
			else if(player < 1 
					&& !world[y][x].hasFact(Facts.Pit)
					&& !world[y][x].hasFact(Facts.Wampus)
					)
			{
				playerX = x;
				playerY = y;
				player++;
			}
		} while (pits < 3 || wampus < 1 || gold < 1 || player < 1);
		
		knowledgeBoard[playerY][playerX].copy(world[playerY][playerX]);
		knowledgeBoard[playerY][playerX].setVisited(true);
		this.pits[y][x] = false;
		this.wampus[y][x] = false;
		
		surroundKnownFacts(world);
		deduceKnowledge();
	}
	
	public void handleClick(double mouseX, double mouseY, boolean leftClick)
	{
		byte x = (byte) (mouseX/100);
		byte y = (byte) (mouseY/100);

		if(leftClick && validClick(x, y))
		{
			playerX = x;
			playerY = y;
			knowledgeBoard[playerY][playerX].copy(world[playerY][playerX]);
			knowledgeBoard[playerY][playerX].setVisited(true);
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Pit))
				this.pits[y][x] = false;
			else
			{
				playerDead = true;
				printMessage("You fell in a pit", Color.RED);
			}
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Breeze))
				surroundWith(this.pits, false, x, y);
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Wampus))
				this.wampus[y][x] = false;
			else
			{
				playerDead = true;
				printMessage("You were etan by a wampus", Color.RED);
			}
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Stench))
				surroundWith(this.wampus, false, x, y);
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Gold))
				this.gold[playerY][playerX] = false;
			else
			{
				world[y][x].removeFact(Facts.Gold);
				unSurround(world, Facts.Glitter, x, y);
				
				knowledgeBoard[y][x].removeFact(Facts.Gold);
				unSurround(knowledgeBoard, Facts.Glitter, x, y);
				
				printGameBoard();
				printKnowledgeBoard();
				printMessage("You Took the Gold !", Color.GREEN);
			}
			
			if(!knowledgeBoard[playerY][playerX].hasFact(Facts.Glitter))
				surroundWith(this.gold, false, x, y);
			
			
			deduceKnowledge();
			printGameBoard();
			printKnowledgeBoard();
		}
		else if(!leftClick && validClick(x, y))
		{
			if(!playerDead && world[y][x].hasFact(Facts.Wampus) && bullets > 0)
			{
				printMessage("You Defeated the Wampus", Color.GREEN);
				world[y][x].removeFact(Facts.Wampus);
				unSurround(world, Facts.Stench, x, y);
				
				knowledgeBoard[y][x].removeFact(Facts.Wampus);
				unSurround(knowledgeBoard, Facts.Stench, x, y);
			}
			
			if(!playerDead && bullets > 0)
			{
				bullets--;
				printGameBoard();
				printKnowledgeBoard();
			}
			else if(!playerDead && bullets == 0)
				printMessage("You ran out of bullets", Color.RED);
			else if(playerDead)
				printMessage("Cant shoot, you are defeated", Color.RED);
			
			printBullets();
		}
	}
	
	private void deduceKnowledge()
	{
		for(byte y = 0; y < 4; y++) 
			for(byte x = 0; x < 4; x++)
			{
				if(count(Facts.Wampus) == 0)
					checkTruthTable(wampus, x, y, Facts.Wampus, Facts.Stench, true);
				
				if(count(Facts.Gold) == 0)
					checkTruthTable(gold, x, y, Facts.Gold, Facts.Glitter, true);
				
				if(count(Facts.Pit) < 3)
					checkTruthTable(pits, x, y, Facts.Pit, Facts.Breeze, false);
			}
		
		surroundKnownFacts(knowledgeBoard);
	}
	
	private void checkTruthTable(Boolean[][] table, byte x, byte y, Facts fact, Facts indication, Boolean checkDiagonal)
	{
		if(knowledgeBoard[y][x].hasFact(indication))
		{
			byte possible = 0;
			
			if(y-1 >= 0 && table[y-1][x] == true)
				possible++;
			
			if(y+1 < 4 && table[y+1][x] == true)
				possible++;
			
			if(x-1 >= 0 && table[y][x-1] == true)
				possible++;
			
			if(x+1 < 4 && table[y][x+1] == true)
				possible++;
			
			if(possible == 1)
			{
				if(y-1 >= 0 && table[y-1][x] == true)
					knowledgeBoard[y-1][x].addFact(fact);
				
				if(y+1 < 4 && table[y+1][x] == true)
					knowledgeBoard[y+1][x].addFact(fact);
				
				if(x-1 >= 0 && table[y][x-1] == true)
					knowledgeBoard[y][x-1].addFact(fact);
				
				if(x+1 < 4 && table[y][x+1] == true)
					knowledgeBoard[y][x+1].addFact(fact);
			}
			
			if(checkDiagonal)
			{		
				if(y+1 < 4 && x+1 < 4 && knowledgeBoard[y+1][x+1].hasFact(indication))
				{	
					if(table[y+1][x] == false && table[y][x+1] == true)
						knowledgeBoard[y][x+1].addFact(fact);
					
					if(table[y+1][x] == true && table[y][x+1] == false)
						knowledgeBoard[y+1][x].addFact(fact);
				}
				
				if(y+1 < 4 && x-1 >= 0 && knowledgeBoard[y+1][x-1].hasFact(indication))
				{
					System.out.println(x+","+y+" : ");
					if(table[y+1][x] == false && table[y][x-1] == true)
						knowledgeBoard[y][x-1].addFact(fact);
					
					if(table[y+1][x] == true && table[y][x-1] == false)
						knowledgeBoard[y+1][x].addFact(fact);
				}
				
				if(x+2 < 4 && knowledgeBoard[y][x+2].hasFact(indication))
					knowledgeBoard[y][x+1].addFact(fact);
				
				if(y+2 < 4 && knowledgeBoard[y+2][x].hasFact(indication))
					knowledgeBoard[y+2][x].addFact(fact);
				
			}
		}
	}
	
	private void surroundKnownFacts(Room[][] board)
	{
		for(byte y = 0; y < 4; y++) 
			for(byte x = 0; x < 4; x++)
			{
				if(board[y][x].hasFact(Facts.Pit))
					surroundWith(board, Facts.Breeze, x, y);
				
				if(board[y][x].hasFact(Facts.Wampus))
					surroundWith(board, Facts.Stench, x, y);
				
				if(board[y][x].hasFact(Facts.Gold))
					surroundWith(board, Facts.Glitter, x, y);
			}	
	}
	
		
	private void printKnowledgeBoard()
	{
		for(byte y = 0; y < 4; y++) 
		{
			for(byte x = 0; x < 4; x++)
			{
				graphicsContext.setStroke(Color.BROWN);
				graphicsContext.strokeRect(x*100, y*100, 100, 100);
				if(isPlayer(x, y))
				{
					graphicsContext.setFill(playerDead ? Color.RED : Color.GREEN);
					graphicsContext.fillRect(x*100, y*100, 100, 100);
					graphicsContext.setFill(Color.WHITE);
					graphicsContext.fillText(knowledgeBoard[y][x]+"", x*100 + 20, y*100 + 40);
				}
				else
				{
					graphicsContext.setFill(Color.WHITE);
					graphicsContext.fillRect(x*100, y*100, 100, 100);
					graphicsContext.setFill(Color.BLACK);
					graphicsContext.fillText(knowledgeBoard[y][x]+"", x*100 + 20, y*100 + 40);
				}
				
			}
		}
	}
	
	private void printGameBoard()
	{
		int xOffset = 450;
		graphicsContext.setFill(Color.GREEN);
		graphicsContext.fillRect(xOffset,0, 4*75, 4*75);
		for(byte y = 0; y < 4; y++) 
		{
			for(byte x = 0; x < 4; x++)
			{	
				graphicsContext.setStroke(Color.BROWN);
				graphicsContext.strokeRect(x*75 + xOffset, y*75, 75, 75);
				graphicsContext.setFill(Color.WHITE);
				graphicsContext.fillText(world[y][x]+"", x*75 + xOffset + 10, y*75 + 30);
			}
		}
	}
	
	private void printMessage(String message, Paint color)
	{
		graphicsContext.setFill(Color.WHITE);
		graphicsContext.fillRect(450, 295,300, 30);
		
		graphicsContext.setFill(color);
		graphicsContext.fillText(message, 450, 320);
	}
	
	private void printBullets()
	{
		graphicsContext.setFill(Color.WHITE);
		graphicsContext.fillRect(450, 345, 85, 20);
		
		graphicsContext.setFill(Color.BLACK);
		graphicsContext.fillText("Bullets: "+bullets,450, 360);
	}
	
	private void surroundWith(Room[][] board, Facts fact, byte x, byte y)
	{
		if(y-1 >= 0)
			board[y-1][x].addFact(fact);
		
		if(y+1 < 4)
			board[y+1][x].addFact(fact);
		
		if(x-1 >= 0)
			board[y][x-1].addFact(fact);
		
		if(x+1 < 4)
			board[y][x+1].addFact(fact);
	}
	
	private void surroundWith(Boolean[][] board, boolean b, byte x, byte y)
	{
		if(y-1 >= 0)
			board[y-1][x] = b;
		
		if(y+1 < 4)
			board[y+1][x] = b;
		
		if(x-1 >= 0)
			board[y][x-1] = b;
		
		if(x+1 < 4)
			board[y][x+1] = b;
	}
	
	private void unSurround(Room[][] board, Facts fact, byte x, byte y)
	{
		if(y-1 >= 0)
			board[y-1][x].removeFact(fact);
		
		if(y+1 < 4)
			board[y+1][x].removeFact(fact);
		
		if(x-1 >= 0)
			board[y][x-1].removeFact(fact);
		
		if(x+1 < 4)
			board[y][x+1].removeFact(fact);
	}
	
	private boolean isPlayer(byte x, byte y)
	{
		return playerX == x && playerY == y;
	}
	

	
	public boolean validClick(byte x, byte y)
	{
		if(y-1 >= 0 && isPlayer(x, (byte) (y-1)))
			return true;
				
		if(y+1 < 4 && isPlayer(x, (byte) (y+1)))
			return true;
		
		if(x-1 >= 0 && isPlayer((byte) (x-1), y))
			return true;
		
		if(x+1 < 4 && isPlayer((byte) (x+1), y))
			return true;
		
		return false;
	}
	
	private int count(Facts fact)
	{
		int count = 0;
		for(byte y = 0; y < 4; y++) 
			for(byte x = 0; x < 4; x++)
				if(knowledgeBoard[y][x].hasFact(fact))
					count++;
		return count;
	}
}